package com.c4.mavenJUnit.UD21.dto;

import static org.junit.jupiter.api.Assertions.*;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import com.c4.mavenJUnit.UD21.dto.Geometria;

class GeometriaTest {

	Geometria geo;
	
	@BeforeEach
	public void before() {
		geo = new Geometria();
	}
	

	// TEST metodo area del cuadrado
	@Test
	public void areaCuadrado() {
		int resultado = geo.areacuadrado(5);
		int esperado = 25;
		assertEquals(esperado, resultado);
	}
	
	// TEST tiempo de ejecucion del area del cuadrado
	@Test
	@Timeout(value = 1, unit = TimeUnit.SECONDS)
	public void testTiempoAreaCuadrado() {
		geo.areacuadrado(50);
	}
		
	// TEST metodo area del circulo
	@Test
	public void areaCirculo() {
		double resultado = geo.areaCirculo(5);
		double esperado = 78.53;
		double delta = 0.5;
		assertEquals(esperado, resultado, delta);
	}
	
	// TEST tiempo de ejecucion del area del circulo
	@Test
	@Timeout(value = 1, unit = TimeUnit.SECONDS)
	public void testTiempoAreaCirculo() {
		geo.areaCirculo(50);
	}
	
	// TEST metodo area del triangulo
	@Test
	public void areatriangulo () {
		int resultado = geo.areatriangulo(10, 5);
		int esperado = 25;
		assertEquals(esperado, resultado);
	}
	
	// TEST tiempo de ejecucion del area del triangulo
	@Test
	@Timeout(value = 1, unit = TimeUnit.SECONDS)
	public void testTiempoAreaTriangulo() {
		geo.areatriangulo(50, 50);
	}
	
	// TEST metodo area del rectangulo
	@Test
	public void arearectangulo() {
		int resultado = geo.arearectangulo(10, 5);
		int esperado = 50;
		assertEquals(esperado, resultado);
	}
	
	// TEST tiempo de ejecucion del area del rectangulo
	@Test
	@Timeout(value = 1, unit = TimeUnit.SECONDS)
	public void testTiempoAreaRectangulo() {
		geo.arearectangulo(50, 50);
	}
	
	// TEST metodo area del pentagono
	@Test
	public void areapentagono() {
		int resultado = geo.areapentagono(10, 5);
		int esperado = 25;
		assertEquals(esperado, resultado);
	}
	
	// TEST tiempo de ejecucion del area del pentagono
	@Test
	@Timeout(value = 1, unit = TimeUnit.SECONDS)
	public void testTiempoAreaPentagono() {
		geo.areapentagono(50, 50);
	}
	
	// TEST metodo area del rombo
	@Test
	public void arearombo() {
		int resultado = geo.arearombo(10, 5);
		int esperado = 25;
		assertEquals(esperado, resultado);
	}
	
	// TEST tiempo de ejecucion del area del rombo
	@Test
	@Timeout(value = 1, unit = TimeUnit.SECONDS)
	public void testTiempoAreaRombo() {
		geo.arearombo(50, 50);
	}
	
	// TEST metodo area del romboide
	@Test
	public void arearomboide() {
		int resultado = geo.arearomboide(1,1);
		int esperado = 1;
		assertEquals(esperado, resultado);
	}
	
	// TEST tiempo de ejecucion del area del romboide
	@Test
	@Timeout(value = 1, unit = TimeUnit.SECONDS)
	public void testTiempoAreaRomboide() {
		geo.arearomboide(50, 50);
	}
	 
	// TEST metodo area del trapecio
	@Test
	public void areatrapecio() {
		int resultado = geo.areatrapecio(5,5,5);
		int esperado = 25;
		assertEquals(esperado, resultado);
	}
	
	// TEST tiempo de ejecucion del area del trapecio
	@Test
	@Timeout(value = 1, unit = TimeUnit.SECONDS)
	public void testTiempoAreaTrapecio() {
		geo.areatrapecio(50, 50, 50);
	}
	
	// TEST selector de figuras
	@Test
	public void figura() {
		String resultado = geo.figura(4);
		String esperado = "Rectangulo";
		assertEquals(esperado, resultado);
	}
	
	// TEST tiempo de ejecucion del selector de figuras
	@Test
	@Timeout(value = 1, unit = TimeUnit.SECONDS)
	public void testTiempoFigura() {
		geo.figura(4);
	}
	
	
	/**
	// TEST tiempo de ejecucion
	@Test
	@Timeout(value = 1, unit = TimeUnit.SECONDS)
	public void testTiempo() {
		geo.();
	}
	**/
	
	/**
	// TEST 
	@Test
	public void () {
		Geometria g1 = new Geometria();
		int resultado = g1.();
		int esperado = ;
		assertEquals(esperado, resultado);
	}
	**/
}
