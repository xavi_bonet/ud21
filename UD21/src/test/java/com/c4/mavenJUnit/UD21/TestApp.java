package com.c4.mavenJUnit.UD21;

import com.c4.mavenJUnit.UD21.dto.Geometria;

public class TestApp 
{
    public static void main( String[] args )
    {
	   //Creacion de Objeto Circulo
	   Geometria gm=new Geometria(2);
	   double areaCirc= gm.areaCirculo(2);
	   gm.setArea(areaCirc);    
	   System.out.println(gm);
    }
    
}
